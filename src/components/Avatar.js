import React from 'react';
import { StyledAvatar } from '../styled/Avatar';

export default function Avatar(props) {
  return (
    <StyledAvatar>
      <img src={props.image}></img>
    </StyledAvatar>
  );
}
