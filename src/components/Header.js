import React from 'react';
import { StyledHeader } from '../styled/Header';

export default function Header() {
  return (
    <StyledHeader>
      <span>Contact List</span>
    </StyledHeader>
  );
}
