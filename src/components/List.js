import React, { useState, useEffect, useCallback } from 'react';
import Result from './Result';
import SearchBar from './SearchBar';
import {StyledList} from '../styled/List'

export default function List() {
  const [contacts, setSontacts] = useState([]);
  const [contactsToRender, setSontactsToRender] = useState([]);
  const [checkedContacts, setCheckedContacts] = useState([]);

  const searchHandler = (e) => {
    const filteredContacts = contacts.filter(
      (contact) =>
        contact.last_name
          .toLowerCase()
          .includes(e.target.value.toLowerCase()) ||
        contact.first_name.toLowerCase().includes(e.target.value.toLowerCase())
    );
    setSontactsToRender(() => filteredContacts);

    console.log(checkedContacts);
    
  };

  useEffect(() => {
    checkedContacts.forEach((contact) => {
      if (document.getElementById(contact) === null) {
      } else {
        document.getElementById(contact).checked = true;
      }
    });
    return () => {
      
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchHandler])

  const checkboxHandler = useCallback(
    (e) => {
      if (e.target.checked) {
        if (checkedContacts === []) {
          localStorage.input = e.target.is(':checked');
          setCheckedContacts((prev) => e.target.id);
        } else {
          setCheckedContacts((prev) => [...prev, e.target.id]);
        }
      } else {
        setCheckedContacts((prev) =>
          prev.filter((contact) => contact !== e.target.id)
        );
      }
    },
    [checkedContacts]
  );

  useEffect(() => {
    console.log(checkedContacts);
    return () => {};
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checkboxHandler]);

  useEffect(() => {
    const getContacts = async () => {
      try {
        const fetchedContacts = await fetch(
          'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
        );
        const contactList = await fetchedContacts.json();
        const sortedList = contactList.sort((a, b) => {
          const nameA = a.last_name.toUpperCase(); // ignore upper and lowercase
          const nameB = b.last_name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // names must be equal
          return 0;
        });
        setSontacts(() => sortedList);
        setSontactsToRender(() => sortedList);
      } catch (error) {
        console.error(error);
      }
    };
    getContacts();
  }, []);

  return (
    <StyledList>
      <SearchBar searchHandler={searchHandler}></SearchBar>
      {contactsToRender.map((contact) => (
        <Result
          checkedContacts={checkedContacts}
          checkboxHandler={checkboxHandler}
          id={contact.id}
          key={contact.id}
          image={contact.avatar}
          firstName={contact.first_name}
          lastName={contact.last_name}
        ></Result>
      ))}
    </StyledList>
  );
}
