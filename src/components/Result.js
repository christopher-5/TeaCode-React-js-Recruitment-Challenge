import React from 'react';
import Avatar from './Avatar';
import { StyledResult } from '../styled/Result';
import { StyledName } from '../styled/Name';
import { StyledCheckbox } from '../styled/CheckBox';

export default function Result(props) {
  return (
    <StyledResult>
      <Avatar image={props.image}></Avatar>
      <StyledName>
        {props.firstName} {props.lastName}
      </StyledName>
      <StyledCheckbox
        id={props.id}
        type="checkbox"
        onChange={props.checkboxHandler}
      ></StyledCheckbox>
    </StyledResult>
  );
}
