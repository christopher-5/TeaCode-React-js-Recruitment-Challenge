import React from 'react';
import { StyledSearchBar } from '../styled/SearchBar';

export default function searchBar(props) {
  return (
    <StyledSearchBar>
      <input
        type="text"
        placeholder="type who you are looking for"
        onChange={props.searchHandler}
      ></input>
    </StyledSearchBar>
  );
}
