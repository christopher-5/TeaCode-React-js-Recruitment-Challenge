import styled from 'styled-components';

export const StyledAvatar = styled.div`
  position: absolute;
  left: 20px;
  top: 5%;
  grid-column: 1;
  img {
  }
`;
