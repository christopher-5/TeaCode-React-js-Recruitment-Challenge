import styled from 'styled-components';

export const StyledCheckbox = styled.input`
  height: 80%;
  top: 10%;
  margin: 0;
  width: 40px;
  right: 20px;
  position: absolute;
`;
