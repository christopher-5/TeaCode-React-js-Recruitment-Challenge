import styled from 'styled-components';

export const StyledHeader = styled.div`
  span {
    font-size: 3.5rem;
    font-weight: bold;
  }
`;
