import styled from 'styled-components';

export const StyledName = styled.span`
  font-size: 2rem;
  grid-column: 2;
  margin: auto 0;
  text-align: left;
  vertical-align: text-bottom;
`;
