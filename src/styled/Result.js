import styled from 'styled-components';

export const StyledResult = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Staatliches&display=swap');
  font-family: 'Staatliches', cursive;
  border: 3px black solid;
  border-radius: 7px;
  margin: 3vh 7vw;
  width: 86vw;
  min-width: 600px;
  height: 60px;
  position: relative;
  display: grid;
  grid-template-columns: 100px 1fr 50px;
  background: rgb(2, 0, 36);
  background: linear-gradient(
    90deg,
    rgba(2, 0, 36, 1) 0%,
    rgba(164, 164, 164, 1) 0%,
    rgba(0, 212, 255, 1) 100%
  );
  span {
    min-width: 375px;
  }
`;
