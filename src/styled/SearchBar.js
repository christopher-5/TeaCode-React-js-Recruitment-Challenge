import styled from 'styled-components';

export const StyledSearchBar = styled.div`
  border-radius: 7px;
  margin: 3vh 7vw;
  width: 86vw;
  height: 25px;
  position: relative;
  input {
    @import url('https://fonts.googleapis.com/css2?family=Staatliches&display=swap');
    font-family: 'Staatliches', cursive;
    position: absolute;
    left: 0;
    height: 30px;
    width: 100%;
    font-size: 21px;
    border-radius: 7px;
    border: 3px solid black;
  }
`;
